#!/bin/bash
required_libraries_available()
{
  which jq > /dev/null
  if [ $? -gt 0 ]
  then
    echo "install jq https://stedolan.github.io/jq/" >&2
    false
  else
    true
  fi
}
check_for_errors() {
  if [ $? -gt 0 ]
  then
    echo "Curl Problem! ${resp}" >&2
    exit 1
  fi
  error=`echo "${resp}" | jq '.error'`
  if [ "${error}" != "null" ]
  then
    echo "API Problem! ${error}" >&2
    exit 1
  fi
}

create_upload()
{
  resp=`curl -f -s -S -X POST --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${DDS_AUTH_TOKEN}" -d '{"name":"'${original_filename}-${file_number}'","content_type":"audio%2Fmpeg","size":"'${upload_size}'","hash":{"value":"'${upload_md5}'","algorithm":"md5"}}' "${DDS_URL}/api/v1/projects/${project_id}/uploads?test=${TEST_NAME}"`
  check_for_errors
  echo "${resp}" | jq -r '.id'
}

create_chunk()
{
  resp=`curl -f -s -S -X PUT --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${DDS_AUTH_TOKEN}" -d '{"number":"'${number}'","size":"'${upload_size}'","hash":{"value":"'${upload_md5}'","algorithm":"md5"}}' "${DDS_URL}/api/v1/uploads/${upload_id}/chunks?test=${TEST_NAME}"`
  check_for_errors
}

complete_upload()
{
  resp=`curl -f -s -S -X PUT --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${DDS_AUTH_TOKEN}" -d '{"hash":{"value":"'${upload_md5}'","algorithm":"md5"}}' "${DDS_URL}/api/v1/uploads/${upload_id}/complete?test=${TEST_NAME}"`
  check_for_errors
}

create_file()
{
  resp=`curl -f -s -S -X POST --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${DDS_AUTH_TOKEN}" -d '{"parent":{"kind":"dds-project","id":"'${project_id}'"},"upload":{"id":"'${upload_id}'"}}' "${DDS_URL}/api/v1/files?test=${TEST_NAME}"`
  check_for_errors
}

post_files()
{
  original_filename=`uuidgen`
  upload_size=1
  upload_md5='cf23df2207d99a74fbe169e3eba035e633b65d94'

  for file_number in $(seq 1 ${desired_files})
  do
    upload_id=`create_upload`
    number=1
    create_chunk
    complete_upload
    create_file
  done
}

usage="Invalid environment: DDS_URL, DDS_AUTH_TOKEN,and PROJECT_ID are required"
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  if [ -z ${DDS_URL} ]
  then
    echo ${usage} >&2
    exit 1
  fi
  if [ -z ${DDS_AUTH_TOKEN} ]
  then
    echo ${usage} >&2
    exit 1
  fi
  project_id=${PROJECT_ID}
  if [ -z ${project_id} ]
  then
    echo ${usage} >&2
    exit 1
  fi

  desired_files=${FILES_DESIRED}
  if [ -z ${desired_files} ]
  then
    desired_files=1
  fi

  time -p post_files
fi
