#!/bin/bash
required_libraries_available()
{
  which jq > /dev/null
  if [ $? -gt 0 ]
  then
    echo "install jq https://stedolan.github.io/jq/" >&2
    false
  else
    true
  fi
}
check_for_errors() {
  if [ $? -gt 0 ]
  then
    echo "Curl Problem! ${resp}" >&2
    exit 1
  fi
  error=`echo "${resp}" | jq '.error'`
  if [ "${error}" != "null" ]
  then
    echo "API Problem! ${error}" >&2
    exit 1
  fi
}

create_upload()
{
  resp=`curl -f -s -S -X POST --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${DDS_AUTH_TOKEN}" -d '{"name":"'${original_filename}-${file_number}'","content_type":"audio%2Fmpeg","size":"'${upload_size}'","hash":{"value":"'${upload_md5}'","algorithm":"md5"}}' "${DDS_URL}/api/v1/projects/${project_id}/uploads?test=${TEST_NAME}"`
  check_for_errors
}

get_auth_token()
{
  resp=`curl -f -s -S -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d '{"agent_key":"'${AGENT_KEY}'","user_key":"'${USER_KEY}'"}' "${DDS_URL}/api/v1/software_agents/api_token?test=${TEST_NAME}"`
  check_for_errors
  echo "${resp}" | jq -r '.api_token'
}

usage="Invalid environment: DDS_URL, DDS_AUTH_TOKEN or (USER_KEY + AGENT_KEY), and PROJECT_ID are required"
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  if [ -z "${DDS_URL}" ]
  then
    echo ${usage} >&2
    exit 1
  fi
  if [ -z "${DDS_AUTH_TOKEN}" ] && [ -z "${USER_KEY}" ] && [ -z "${AGENT_KEY}" ]
  then
    echo ${usage} >&2
    exit 1
  fi

  if [ -z "${DDS_AUTH_TOKEN}" ]
  then
    if [ -z "${USER_KEY}" ] || [ -z "${AGENT_KEY}" ]
    then
      echo ${usage} >&2
      exit 1
    fi
    DDS_AUTH_TOKEN=$(get_auth_token)
  fi

  project_id=${PROJECT_ID}
  if [ -z "${project_id}" ]
  then
    echo ${usage} >&2
    exit 1
  fi

  original_filename=`uuidgen`
  upload_size=1
  upload_md5='cf23df2207d99a74fbe169e3eba035e633b65d94'
  file_number=1
  time -p create_upload
fi
