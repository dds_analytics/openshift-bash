export MOCK_CURL_TEMPFILE
function mock_curl() {
  curl_mocks[${#curl_mocks[@]}]=$(join_by $'\t' "$@")
  [ -n "$MOCK_CURL_TEMPFILE" ] || MOCK_CURL_TEMPFILE="$(mktemp ${BATS_TMPDIR}/mock_curl_$(printf %03d ${BATS_TEST_NUMBER}).XXXX)"
  persist_curl_mocks
}

function persist_curl_mocks() {
  declare -p curl_mocks > "$MOCK_CURL_TEMPFILE"
}

teardown_curl_mocks() {
  if [ $BATS_TEST_COMPLETED ]; then
    rm -f "$MOCK_CURL_TEMPFILE"
  else
    echo "** Did not delete $MOCK_CURL_TEMPFILE, as test failed **"
  fi
}

function join_by { local IFS="$1"; shift; echo "$*"; }

function curl() {
  url=${!#}
  echo "Curling '$*'" >&2;
  if [ -n "${MOCK_CURL_TEMPFILE}" ]
  then
    . "${MOCK_CURL_TEMPFILE}"
  fi
  if [ ${#curl_mocks[@]} -gt 0 ]
  then
    for index in $(seq 1 ${#curl_mocks[@]})
    do
      #mock=$'url\tresponse'
      mock="${curl_mocks[$index-1]}"
      match_length=$(expr "$mock" : $url$'\t')
      if [ $match_length -gt 0 ]
      then
        unset curl_mocks[$index-1]
        curl_mocks=("${curl_mocks[@]}")
        persist_curl_mocks
        echo "${mock:$match_length}"
        return 0
      fi
    done
  fi
  echo "curl error"
  return 22
}
export -f curl persist_curl_mocks
