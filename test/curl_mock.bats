#!/usr/bin/env ./test/libs/bats/bin/bats

load 'libs/bats-support/load'
load 'libs/bats-assert/load'
load 'helpers'
load 'curl_mock'

@test "mock_curl adds tab delemited string to curl_mocks" {
  mock_curl foo bar baz blah
  [ "${curl_mocks[0]}" = $'foo\tbar\tbaz\tblah' ]
}

@test "mock_curl handles strings with spaces" {
  mock_curl foo 'bar baz' blah
  [ "${curl_mocks[0]}" = $'foo\tbar baz\tblah' ]
}

@test "mock_curl called twice" {
  mock_curl foo bar baz blah
  mock_curl foo 'bar baz' blah
  [ "${curl_mocks[0]}" = $'foo\tbar\tbaz\tblah' ]
  [ "${curl_mocks[1]}" = $'foo\tbar baz\tblah' ]
}

@test "join_by handles strings with spaces" {
  joined=$(join_by $'\t' foo 'bar baz' blah)
  [ "${joined}" = $'foo\tbar baz\tblah' ]
}

@test "curl with curl_mocks" {
  curl_mocks=()
  mock_curl url_one bar baz blah
  mock_curl "${DDS_URL}/url_two" '{"key": "value"}'
  run curl foo bar baz "${DDS_URL}/url_two"
  assert_output -p $'{"key": "value"}'
  refute_output -p $'bar\tbaz\tblah'
}

@test "curl only runs a mock once" {
  function run_curl_twice() {
    curl foo bar baz url_two
    curl foo bar baz url_two
  }
  curl_mocks=()
  mock_curl url_one bar baz blah
  mock_curl url_two 'bar baz' foo
  run run_curl_twice
  assert_failure
  assert_output -e $'(bar baz\tfoo.*){1}'
  refute_output -e $'(bar baz\tfoo.*){2}'
  refute_output -p $'bar\tbaz\tblah'
}

@test "curl runs multiple mocks" {
  function run_curl_twice() {
    curl baz url_one
    curl baz url_one
    curl baz url_two
  }
  curl_mocks=()
  mock_curl url_one bar baz blah
  mock_curl url_two 'bar baz' foo
  run run_curl_twice
  assert_success
  assert_output -p $'bar baz\tfoo'
  assert_output -p $'bar\tbaz\tblah'
}
