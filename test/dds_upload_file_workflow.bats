#!/usr/bin/env ./test/libs/bats/bin/bats

load 'libs/bats-support/load'
load 'libs/bats-assert/load'
load 'helpers'
load 'curl_mock'

expected_test_name="foo"
export DDS_URL=http://localhost
export DDS_AUTH_TOKEN=foo
export PROJECT_ID=$(uuidgen)
export TEST_NAME="${expected_test_name}"

teardown() {
  teardown_curl_mocks
}

script_name='dds_upload_file_workflow.sh'
script_bin="./bin/${script_name}"
curl_standard_params='-f -s -S -X'
curl_params_post="${curl_standard_params} POST"
curl_put_params="${curl_standard_params} PUT"
env_error_message="Invalid environment: DDS_URL, DDS_AUTH_TOKEN,and PROJECT_ID are required"

expected_upload_id="1"
function upload_url() {
  echo "${DDS_URL}/api/v1/projects/${PROJECT_ID}/uploads"
}

function chunk_url() {
  echo "${DDS_URL}/api/v1/uploads/${expected_upload_id}/chunks"
}

function complete_upload_url() {
  echo "${DDS_URL}/api/v1/uploads/${expected_upload_id}/complete"
}

function files_url() {
  echo "${DDS_URL}/api/v1/files"
}

@test "${script_name} requires DDS_URL" {
  unset DDS_URL

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} requires PROJECT_ID" {
  unset PROJECT_ID

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} requires DDS_AUTH_TOKEN" {
  unset DDS_AUTH_TOKEN

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "#create_upload calls curl and returns error" {
  project_id=${PROJECT_ID}
  source $script_bin
  run create_upload

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(upload_url)\?test=${expected_test_name}'"
  assert_line -n 1 "Curl Problem! curl error"
}

@test "#create_upload calls curl and returns api error" {
  project_id=${PROJECT_ID}
  mock_curl "$(upload_url)?test=${expected_test_name}" '{"error": "boom"}'

  source $script_bin
  run create_upload

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(upload_url)\?test=${expected_test_name}'"

  assert_line -n 1 'API Problem! "boom"'
}

@test "#create_upload calls curl and returns upload with id" {
  project_id=${PROJECT_ID}
  mock_curl "$(upload_url)?test=${expected_test_name}" '{"id": "1"}'

  source $script_bin
  run create_upload

  assert_success
  assert_line -n 0 -e "Curling '${curl_params_post}[^']*$(upload_url)\?test=${expected_test_name}'"
  assert_line -n 1 "1"
}

@test "#create_chunk calls curl and returns error" {
  upload_id=${expected_upload_id}
  source $script_bin
  run create_chunk

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(chunk_url)\?test=${expected_test_name}'"
  assert_line -n 1 "Curl Problem! curl error"
}

@test "#create_chunk calls curl and returns api error" {
  upload_id=${expected_upload_id}
  mock_curl "$(chunk_url)?test=${expected_test_name}" '{"error": "boom"}'

  source $script_bin
  run create_chunk

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(chunk_url)\?test=${expected_test_name}'"

  assert_line -n 1 'API Problem! "boom"'
}

@test "#create_chunk calls curl and returns success" {
  upload_id=${expected_upload_id}
  mock_curl "$(chunk_url)?test=${expected_test_name}" '{"url": "url"}'

  source $script_bin
  run create_chunk

  assert_success
  assert_line -n 0 -e "Curling '${curl_put_params}[^']*$(chunk_url)\?test=${expected_test_name}'"
}

@test "#complete_upload calls curl and returns error" {
  upload_id=${expected_upload_id}
  source $script_bin
  run complete_upload

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(complete_upload_url)\?test=${expected_test_name}'"
  assert_line -n 1 "Curl Problem! curl error"
}

@test "#complete_upload calls curl and returns api error" {
  upload_id=${expected_upload_id}
  mock_curl "$(complete_upload_url)?test=${expected_test_name}" '{"error": "boom"}'

  source $script_bin
  run complete_upload

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(complete_upload_url)\?test=${expected_test_name}'"

  assert_line -n 1 'API Problem! "boom"'
}

@test "#complete_upload calls curl and returns success" {
  upload_id=${expected_upload_id}
  mock_curl "$(complete_upload_url)?test=${expected_test_name}" '{"id": "1"}'

  source $script_bin
  run complete_upload

  assert_success
  assert_line -n 0 -e "Curling '${curl_put_params}[^']*$(complete_upload_url)\?test=${expected_test_name}'"
}

# create_file
@test "#create_file calls curl and returns error" {
  source $script_bin
  run create_file

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(files_url)\?test=${expected_test_name}'"
  assert_line -n 1 "Curl Problem! curl error"
}

@test "#create_file calls curl and returns api error" {
  mock_curl "$(files_url)?test=${expected_test_name}" '{"error": "boom"}'

  source $script_bin
  run create_file

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(files_url)\?test=${expected_test_name}'"

  assert_line -n 1 'API Problem! "boom"'
}

@test "#create_file calls curl and returns success" {
  mock_curl "$(files_url)?test=${expected_test_name}" '{"id": "1"}'

  source $script_bin
  run create_file

  assert_success
  assert_line -n 0 -e "Curling '${curl_params_post}[^']*$(files_url)\?test=${expected_test_name}'"
}

@test "#post_files" {
  project_id=${PROJECT_ID}
  desired_files=2
  unexpected_desired_files=3
  for iter in `seq 1 ${desired_files}`
  do
    expected_upload_id="${iter}"
    mock_curl "$(upload_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
    mock_curl "$(chunk_url)?test=${expected_test_name}" '{"url": "url"}'
    mock_curl "$(complete_upload_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
    mock_curl "$(files_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
  done

  source ${script_bin}
  run post_files
  assert_success
  assert_output -e "Curling '${curl_params_post}[^']*-${desired_files}[^']*$(upload_url)\?test=${expected_test_name}'"
  refute_output -e "Curling '${curl_params_post}[^']*-${unexpected_desired_files}[^']*$(upload_url)\?test=${expected_test_name}'"
}

@test "FILES_DESIRED environment variable is not set" {
  expected_default_desired_files="1"
  unexpected_default_desired_files="2"
  for iter in `seq 1 ${expected_default_desired_files}`
  do
    expected_upload_id="${iter}"
    mock_curl "$(upload_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
    mock_curl "$(chunk_url)?test=${expected_test_name}" '{"url": "url"}'
    mock_curl "$(complete_upload_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
    mock_curl "$(files_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
  done

  run ${script_bin}
  assert_success
  assert_output -e "Curling '${curl_params_post}[^']*-${expected_default_desired_files}[^']*$(upload_url)\?test=${expected_test_name}'"
  refute_output -e "Curling '${curl_params_post}[^']*-${unexpected_default_desired_files}[^']*$(upload_url)\?test=${expected_test_name}'"
  assert_output -e "real"
}

@test "FILES_DESIRED environment variable is set" {
  expected_desired_files="2"
  unexpected_file_number="12353"
  export FILES_DESIRED=${expected_desired_files}
  for iter in `seq 1 ${expected_desired_files}`
  do
    expected_upload_id="${iter}"
    mock_curl "$(upload_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
    mock_curl "$(chunk_url)?test=${expected_test_name}" '{"url": "url"}'
    mock_curl "$(complete_upload_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
    mock_curl "$(files_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
  done

  run ${script_bin}
  assert_success
  assert_output -e "Curling '${curl_params_post}[^']*-${expected_desired_files}[^']*$(upload_url)\?test=${expected_test_name}'"
  refute_output -e "Curling '${curl_params_post}[^']*-${unexpected_file_number}[^']*$(upload_url)\?test=${expected_test_name}'"
  assert_output -e "real"
}
