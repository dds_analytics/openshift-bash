#!/usr/bin/env ./test/libs/bats/bin/bats

load 'libs/bats-support/load'
load 'libs/bats-assert/load'
load 'helpers'
load 'curl_mock'

expected_test_name="foo"
export TEST_NAME="${expected_test_name}"
export DDS_URL=http://localhost
export AGENT_KEY=foo
export USER_KEY=bar
export DDS_AUTH_TOKEN=foo
export PROJECT_ID=$(uuidgen)
expected_upload_id="1"

teardown() {
  teardown_curl_mocks
}

script_name='dds_create_chunk.sh'
script_bin="./bin/${script_name}"
curl_standard_params='-f -s -S -X'
curl_params_post="${curl_standard_params} POST"
curl_put_params="${curl_standard_params} PUT"
env_error_message="Invalid environment: DDS_URL, DDS_AUTH_TOKEN or (USER_KEY + AGENT_KEY), and PROJECT_ID are required"

expected_upload_id="1"

function upload_url() {
  echo "${DDS_URL}/api/v1/projects/${PROJECT_ID}/uploads"
}

function api_token_url() {
  echo "${DDS_URL}/api/v1/software_agents/api_token"
}

function chunk_url() {
  echo "${DDS_URL}/api/v1/uploads/${expected_upload_id}/chunks"
}

@test "${script_name} requires DDS_URL" {
  unset DDS_URL

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} requires PROJECT_ID" {
  unset PROJECT_ID

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} requires DDS_AUTH_TOKEN without USER_KEY and AGENT_KEY" {
  unset DDS_AUTH_TOKEN
  unset USER_KEY
  unset AGENT_KEY

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} requires USER_KEY without DDS_AUTH_TOKEN" {
  unset DDS_AUTH_TOKEN
  unset USER_KEY

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "${script_name} requires AGENT_KEY without DDS_AUTH_TOKEN" {
  unset DDS_AUTH_TOKEN
  unset AGENT_KEY

  run $script_bin

  assert_failure
  assert_output "${env_error_message}"
}

@test "#get_auth_token calls curl and returns error" {
  source $script_bin
  run get_auth_token

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(api_token_url)\?test=${expected_test_name}'"
  assert_line -n 1 "Curl Problem! curl error"
}

@test "#get_auth_token calls curl and returns api error" {
  mock_curl "$(api_token_url)?test=${expected_test_name}" '{"error": "boom"}'

  source $script_bin
  run get_auth_token

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(api_token_url)\?test=${expected_test_name}'"
  assert_line -n 1 'API Problem! "boom"'
}

@test "#get_auth_token calls curl and returns token" {
  unset DDS_AUTH_TOKEN
  mock_curl "$(api_token_url)?test=${expected_test_name}" '{"api_token": "foo"}'

  source $script_bin
  run get_auth_token

  assert_success
  assert_line -n 0 -e "Curling '${curl_params_post}[^']*$(api_token_url)\?test=${expected_test_name}'"
  assert_output -e "foo"
}

@test "#create_upload calls curl and returns error" {
  project_id=${PROJECT_ID}
  source $script_bin
  run create_upload

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(upload_url)\?test=${expected_test_name}'"
  assert_line -n 1 "Curl Problem! curl error"
}

@test "#create_upload calls curl and returns api error" {
  project_id=${PROJECT_ID}
  mock_curl "$(upload_url)?test=${expected_test_name}" '{"error": "boom"}'

  source $script_bin
  run create_upload

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(upload_url)\?test=${expected_test_name}'"

  assert_line -n 1 'API Problem! "boom"'
}

@test "#create_upload calls curl and returns upload with id" {
  project_id=${PROJECT_ID}
  mock_curl "$(upload_url)?test=${expected_test_name}" '{"id": "1"}'

  source $script_bin
  run create_upload

  assert_success
  assert_line -n 0 -e "Curling '${curl_params_post}[^']*$(upload_url)\?test=${expected_test_name}'"
  assert_line -n 1 "1"
}

@test "#create_chunk calls curl and returns error" {
  upload_id=${expected_upload_id}
  source $script_bin
  run create_chunk

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(chunk_url)\?test=${expected_test_name}'"
  assert_line -n 1 "Curl Problem! curl error"
}

@test "#create_chunk calls curl and returns api error" {
  upload_id=${expected_upload_id}
  mock_curl "$(chunk_url)?test=${expected_test_name}" '{"error": "boom"}'

  source $script_bin
  run create_chunk

  assert_failure
  assert_line -n 0 -e "Curling '[^']*$(chunk_url)\?test=${expected_test_name}'"

  assert_line -n 1 'API Problem! "boom"'
}

@test "#create_chunk calls curl and returns success" {
  upload_id=${expected_upload_id}
  mock_curl "$(chunk_url)?test=${expected_test_name}" '{"url": "url"}'

  source $script_bin
  run create_chunk

  assert_success
  assert_line -n 0 -e "Curling '${curl_put_params}[^']*$(chunk_url)\?test=${expected_test_name}'"
}

@test "with USER_KEY and AGENT_KEY" {
  unset DDS_AUTH_TOKEN

  iter=1
  expected_upload_id="${iter}"
  mock_curl "$(api_token_url)?test=${expected_test_name}" '{"api_token": "foo"}'
  mock_curl "$(upload_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
  mock_curl "$(chunk_url)?test=${expected_test_name}" '{"url": "url"}'

  run ${script_bin}
  assert_success
  assert_output -e "Curling '${curl_params_post}[^']*-${iter}[^']*$(upload_url)\?test=${expected_test_name}'"
  assert_output -e "real"
}

@test "with DDS_AUTH_TOKEN" {
  unset USER_KEY
  unset AGENT_KEY
  iter=1
  expected_upload_id="${iter}"
  mock_curl "$(upload_url)?test=${expected_test_name}" "{\"id\": \"${iter}\"}"
  mock_curl "$(chunk_url)?test=${expected_test_name}" '{"url": "url"}'

  run ${script_bin}
  assert_success
  assert_output -e "Curling '${curl_params_post}[^']*-${iter}[^']*$(upload_url)\?test=${expected_test_name}'"
  assert_output -e "real"
}
